describe('Los estudiantes homepage login and register', function(){
	it('Register successful with a valid email', function(){
		cy.visit('https://losestudiantes.co')
		
		//Close pop up and open registration form
		cy.contains('Cerrar').click()
		cy.contains('Ingresar').click()
		
		cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Estudiante")
		cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Sin Registro")
		cy.get('.cajaSignUp').find('input[name="correo"]').click().type('ja.gomez1@uniandes.edu.co')
		cy.get('[name=idPrograma]').select('Ingeniería de Sistemas y Computación')
		cy.get('.cajaSignUp').find('input[name="password"]').click().type("12345678")
		cy.get('.cajaSignUp').find('input[name="acepta"]').click()
		cy.get('.cajaSignUp').contains('Registrarse').click()
		cy.contains('Ocurrió un error activando tu cuenta')
		cy.get('.sweet-alert').contains('Ok').click()
	})

	it('Failed attempt to register already registered email', function(){
		cy.get('.cajaLogIn').find('input[name="correo"]').click().type("mdr.leon10@uniandes.edu.co")
      	cy.get('.cajaLogIn').find('input[name="password"]').click().type("Ml210296")
      	cy.get('.cajaLogIn').contains('Ingresar').click()
      	cy.get('.buscador').find('input[role="combobox"]').type("Jorge Alexander Duitama Castellanos", { force: true })
      	cy.contains("Jorge Alexander Duitama Castellanos - Ingeniería de Sistemas").click()
      	cy.get('.boxElement').find('input[name="id:ISIS1104"]').click()

	})
})