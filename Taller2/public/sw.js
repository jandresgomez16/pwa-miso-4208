'use strict';

// Importing workbox
importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');
if (workbox) {
    console.log(`Yay! Workbox is loaded 🎉`);

    workbox.precaching.precacheAndRoute([
  {
    "url": "images/ic_add_white_24px.svg",
    "revision": "c3379830302abe84f64db87b5bac9faa"
  },
  {
    "url": "images/ic_refresh_white_24px.svg",
    "revision": "f73272d4efd233a85e8c649d26126f01"
  },
  {
    "url": "images/icon_192.jpg",
    "revision": "e68231ea6758940bcbe18275e73a6788"
  },
  {
    "url": "images/icon_512.jpg",
    "revision": "3e6af6535713b8c99f8ab67ab1090149"
  },
  {
    "url": "images/icon.jpg",
    "revision": "491b8452d3f0a4d488b07622eb15ddd1"
  },
  {
    "url": "images/metro.ico",
    "revision": "b076779ef77d7f25f4f4492a42f0ef2d"
  },
  {
    "url": "index.html",
    "revision": "0c2dee7e6b5a5b2c2a1a41b7f5bac4f8"
  },
  {
    "url": "manifest.json",
    "revision": "3c8b450eb573b73efaa06c37ae4458fe"
  },
  {
    "url": "scripts/app.js",
    "revision": "a7d2c4b963371132084285fab4e6158f"
  },
  {
    "url": "service-worker.js",
    "revision": "36b078e7d40705b28b6cfa5502146195"
  },
  {
    "url": "styles/inline.css",
    "revision": "2fa3640041d722a1a3fb5d629dc95829"
  }
]);

    workbox.routing.registerRoute(/\.css$/, 
        new workbox.strategies.CacheFirst({cacheName: 'css-cache',})
    );

    workbox.routing.registerRoute('/', 
        new workbox.strategies.CacheFirst({cacheName: 'site-cache',})
    );

    workbox.routing.registerRoute(/\.js$/, 
        new workbox.strategies.CacheFirst({cacheName: 'js-cache',})
    );

    workbox.routing.registerRoute(/\.(?:png|jpg|jpeg|svg|gif|ico)$/, 
        new workbox.strategies.CacheFirst({cacheName: 'image-cache',})
    );
} else {
    console.log(`Boo! Workbox didn't load 😬`);
}

self.addEventListener('install', (evt) => {
    console.log('[ServiceWorker] Install');
    self.skipWaiting();
});

self.addEventListener('activate', (evt) => {
    console.log('[ServiceWorker] Activate');
    self.clients.claim();
});

self.addEventListener('fetch', (evt) => {
    console.log('[ServiceWorker] Fetch', evt.request.url);
});