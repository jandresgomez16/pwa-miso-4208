'use strict';

// Importing workbox
importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');
if (workbox) {
    console.log(`Yay! Workbox is loaded 🎉`);

    workbox.precaching.precacheAndRoute([]);

    workbox.routing.registerRoute(/\.css$/, 
        new workbox.strategies.CacheFirst({cacheName: 'css-cache',})
    );

    workbox.routing.registerRoute('/', 
        new workbox.strategies.CacheFirst({cacheName: 'site-cache',})
    );

    workbox.routing.registerRoute(/\.js$/, 
        new workbox.strategies.CacheFirst({cacheName: 'js-cache',})
    );

    workbox.routing.registerRoute(/\.(?:png|jpg|jpeg|svg|gif|ico)$/, 
        new workbox.strategies.CacheFirst({cacheName: 'image-cache',})
    );
} else {
    console.log(`Boo! Workbox didn't load 😬`);
}

self.addEventListener('install', (evt) => {
    console.log('[ServiceWorker] Install');
    self.skipWaiting();
});

self.addEventListener('activate', (evt) => {
    console.log('[ServiceWorker] Activate');
    self.clients.claim();
});

self.addEventListener('fetch', (evt) => {
    console.log('[ServiceWorker] Fetch', evt.request.url);
});