(function () {
    'use strict';

    var app = {
        isLoading: true,
        visibleCards: {},
        selectedTimetables: [],
        spinner: document.querySelector('.loader'),
        cardTemplate: document.querySelector('.cardTemplate'),
        container: document.querySelector('.main'),
        addDialog: document.querySelector('.dialog-container')
    };


    /*****************************************************************************
     *
     * Event listeners for UI elements
     *
     ****************************************************************************/

    document.getElementById('butRefresh').addEventListener('click', function () {
        // Refresh all of the metro stations
        app.updateSchedules();
    });

    document.getElementById('butAdd').addEventListener('click', function () {
        // Open/show the add new station dialog
        app.toggleAddDialog(true);
    });

    document.getElementById('butAddCity').addEventListener('click', function () {
        var select = document.getElementById('selectTimetableToAdd');
        var selected = select.options[select.selectedIndex];
        var key = selected.value;
        var label = selected.textContent;
        if (!app.selectedTimetables) {
            app.selectedTimetables = [];
        }
        app.getSchedule(key, label);
        app.selectedTimetables.push({ key: key, label: label });
        app.toggleAddDialog(false);
    });

    document.getElementById('butAddCancel').addEventListener('click', function () {
        // Close the add new station dialog
        app.toggleAddDialog(false);
    });


    /*****************************************************************************
     *
     * Methods to update/refresh the UI
     *
     ****************************************************************************/

    // Toggles the visibility of the add new station dialog.
    app.toggleAddDialog = function (visible) {
        if (visible) {
            app.addDialog.classList.add('dialog-container--visible');
        } else {
            app.addDialog.classList.remove('dialog-container--visible');
        }
    };

    // Updates a timestation card with the latest weather forecast. If the card
    // doesn't already exist, it's cloned from the template.

    app.updateTimetableCard = function (data) {
        var key = data.key;
        var schedules = data.schedules;
        var card = app.visibleCards[key];

        try {
            //Add to IndexedDB
            var request = app.db.transaction(['stations'], 'readwrite').objectStore('stations').put({
                key: data.key,
                label: data.label,
                schedules: data.schedules,
                created: data.created
            })
            request.onsuccess = function (event) {
                console.log('[IndexedDB] add succeded -> ', request.result)
            };
        } catch (err) {
            console.log('DB not opened yet')
        }

        if (!card) {
                var label = data.label.split(', ');
                var title = label[0];
                var subtitle = label[1];
                card = app.cardTemplate.cloneNode(true);
                card.classList.remove('cardTemplate');
                card.querySelector('.label').textContent = title;
                card.querySelector('.subtitle').textContent = subtitle;
                card.removeAttribute('hidden');
                app.container.appendChild(card);
                app.visibleCards[key] = card;
            
        }
        card.querySelector('.card-last-updated').textContent = data.created;

        var scheduleUIs = card.querySelectorAll('.schedule');
        for (var i = 0; i < 4; i++) {
            var schedule = schedules[i];
            var scheduleUI = scheduleUIs[i];
            if (schedule && scheduleUI) {
                scheduleUI.querySelector('.message').textContent = schedule.message;
            }
        }

        if (app.isLoading) {
            app.spinner.setAttribute('hidden', true);
            app.container.removeAttribute('hidden');
            app.isLoading = false;
        }
    };

    /*****************************************************************************
     *
     * Methods for dealing with the model
     *
     ****************************************************************************/


    app.getSchedule = function (key, label) {
        if (!label) return;
        var url = 'https://api-ratp.pierre-grimaud.fr/v3/schedules/' + key;

        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status === 200) {
                    var response = JSON.parse(request.response);
                    var result = {};
                    result.key = key;
                    result.label = label;
                    result.created = response._metadata.date;
                    result.schedules = response.result.schedules;
                    app.updateTimetableCard(result);
                }
            } else {
                // Return a blank time card
                app.updateTimetableCard({
                    key: key,
                    label: label,
                    created: '2017-07-18T17:08:42+02:00',
                    schedules: [
                        { message: "l'infini mn" },
                        { message: "l'infini mn" },
                        { message: "l'infini mn" },
                        { message: "l'infini mn" }
                    ]
                });
            }
        };
        request.open('GET', url);
        request.send();
    };

    // Iterate all of the cards and attempt to get the latest timetable data
    app.updateSchedules = function () {
        app.selectedTimetables.map(function (timeTable) {
            app.getSchedule(timeTable.key, timeTable.label)
        })
    };

    /*
     * Fake timetable data that is presented when the user first uses the app,
     * or when the user has not saved any stations. See startup code for more
     * discussion.
     */

    var initialStationTimetable = {
        key: 'metros/1/bastille/A',
        label: 'Bastille, Direction La Défense',
        created: '2017-07-18T17:08:42+02:00',
        schedules: [
            { message: '0 mn' },
            { message: '2 mn' },
            { message: '5 mn' },
            { message: '7 mn' }
        ]
    };

    app.selectedTimetables = [
        { key: initialStationTimetable.key, label: initialStationTimetable.label }
    ];
    app.selectedTimetables.map(function (timeTable) {
        app.getSchedule(timeTable.key, timeTable.label)
    })

    // IndexedDB setup
    if (indexedDB) {
        var request = indexedDB.open('METRO_DB');
        request.onsuccess = function (event) {
            console.log('[onsuccess]', request.result);
            app.db = event.target.result;

            //Fetch saved stations from DB or use default station
            var fetchAllrequest = app.db.transaction(['stations']).objectStore('stations').getAll()
            fetchAllrequest.onsuccess = function (event) {
                console.log('[IndexedDB] fetch all succeded -> ', fetchAllrequest.result)

                //Update view for all schedules that are not yet on display
                fetchAllrequest.result.map(function (data) {
                    //Verify if the entry already exists
                    let timeTableEntry = app.selectedTimetables.filter(function (timeTable) {
                        return timeTable.key == data.key
                    })
                    if (timeTableEntry.length == 0) {
                        app.selectedTimetables.push({
                            key: data.key,
                            label: data.label
                        })
                    }

                    //Create Cards while offline with saved schedules
                    app.updateTimetableCard(data)
                    app.getSchedule(data.key, data.label)
                })
            };
        };

        request.onerror = function (event) {
            console.log('[onerror]', request.error);
        };

        request.onupgradeneeded = function (event) {
            console.log("[IndexedDB] upgrade needed -> ", event)
            app.db = event.target.result;
            var store = app.db.createObjectStore('stations', { keyPath: 'key' });

            store.transaction.oncomplete = function (event) {
                var stationsObjectStore = app.db.transaction("stations", "readwrite").objectStore("stations");
                app.selectedTimetables.map(function (timeTable) {
                    let mappedData = {
                        key: timeTable.key,
                        label: timeTable.label,
                        created: '2017-07-18T17:08:42+02:00',
                        schedules: [
                            { message: "l'infini mn" },
                            { message: "l'infini mn" },
                            { message: "l'infini mn" },
                            { message: "l'infini mn" }
                        ]
                    }
                    stationsObjectStore.add(mappedData)
                })
            }
        };
    } else {
        console.log('FATAL ERROR -> IndexedDB not available on your browser')
    }
})();
