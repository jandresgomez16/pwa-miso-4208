module.exports = {
  "globDirectory": "public/",
  "globPatterns": [
    "**/*.{svg,jpg,ico,html,json,js,css}"
  ],
  "swDest": "public/sw.js",
  "swSrc": "public/service-worker.js"
};