describe('Los estudiantes under monkeys', function() {
    it('visits los estudiantes and survives monkeys', function() {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.wait(1000);
        randomEventDispatcher(1000);
    })
})

let timeBetweenEvents = 1000;

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
};

function randomEventDispatcher(monkeysLeft) {
    let monLeft = monkeysLeft;
    let events = [randomLinkClick, randomFill, randomButtonClick, randomSelector];
    if(monLeft > 0) {
        let posEvent = getRandomInt(0, events.length);
        return events[posEvent](monLeft).then((ml) => {
            cy.wait(timeBetweenEvents);
            console.log(`ml = ${ml}`)
            randomEventDispatcher(ml);
        }).catch((err) => console.log(err));
    }
}

function randomLinkClick(monkeysLeft) {
    return new Promise((resolve, reject) => {
        let monLeft = monkeysLeft - 1;
        try { 
            cy.get('a').then($links => {
                var randomLink = $links.get(getRandomInt(0, $links.length));
                if(!Cypress.dom.isHidden(randomLink)) {
                    cy.wrap(randomLink).click({force: true});
                }
                resolve(monLeft);
            });
        } catch(err) {
            console.log(err);
            resolve(monLeft);
        };
    });

    
}

function randomFill(monkeysLeft) {
    return new Promise((resolve, reject) => {
        let monLeft = monkeysLeft - 1;
        try { 
            if(Cypress.$('input[type="text"]').length > 0) {
                cy.get('input[type="text"]').then($inputs => {
                    let ranInput = $inputs.get(getRandomInt(0, $inputs.length));
                    if(!Cypress.dom.isHidden(ranInput)) {
                        cy.wrap(ranInput).type('random input', {force: true});
                    }
                    resolve(monLeft);
                });
            } else {
                console.log('NOT FOUND');
                resolve(monLeft);
            }
        } catch(err) {
            console.log(err);
            resolve(monLeft);
        };
    });
}

function randomButtonClick(monkeysLeft) {
    return new Promise((resolve, reject) => {
        let monLeft = monkeysLeft - 1;
        try { 
            if(Cypress.$('button').length > 0) {
                cy.get('button').then($buttons => {
                    let ranButton = $buttons.get(getRandomInt(0, $buttons.length));
                    if(!Cypress.dom.isHidden(ranButton)) {
                        cy.wrap(ranButton).click({ force: true})
                    }
                    resolve(monLeft);
                });
            } else {
                console.log('NOT FOUND');
                resolve(monLeft);
            }
        } catch(err) {
            console.log(err);
            resolve(monLeft);
        };
    });
}

function randomSelector(monkeysLeft) {
    return new Promise((resolve, reject) => {
        let monLeft = monkeysLeft - 1;
        try { 
            if(Cypress.$('select').length > 0) {
                cy.get('select').then($selects => {
                    let ranSelect = $selects.get(getRandomInt(0, $selects.length));
                    let ranOpt = ranSelect.options[getRandomInt(0, ranSelect.options.length)].value;
            
                    if(!Cypress.dom.isHidden(ranSelect)) {
                        cy.wrap(ranSelect).select(ranOpt, { force: true });
                    }
                    resolve(monLeft);
                });
            } else {
                console.log('NOT FOUND');
                resolve(monLeft);
            }
        } catch(err) {
            console.log(err);
            resolve(monLeft);
        };
    });
}