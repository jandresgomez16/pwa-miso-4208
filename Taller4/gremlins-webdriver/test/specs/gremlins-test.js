function loadScript(callback) {
  var s = document.createElement('script');
  s.src = 'https://rawgithub.com/marmelab/gremlins.js/master/gremlins.min.js';
  if (s.addEventListener) {
    s.addEventListener('load', callback, false);
  } else if (s.readyState) {
    s.onreadystatechange = callback;
  }
  document.body.appendChild(s);
}

function unleashGremlins(ttl, callback) {
  function stop() {
    horde.stop();
    callback();
  }

  let newCleanClickGremlin = window.gremlins.species.clicker().clickTypes(['click']).canClick(function(el) {
    return (el.tagName == "A" ||
        el.tagName == "BUTTON") && !el.hidden;
  });

  let newFormFillingGremlin = window.gremlins.species.formFiller().canFillElement(function(el) {
    return (el.tagName == "INPUT" ||
    el.type == "text" ||
    el.type == "password" ||
    el.type == "email") && !el.hidden;
  });

  let canScrollGremlin = window.gremlins.species.toucher();

  //Add new gremlins to the horde
  let horde = window.gremlins.createHorde()
  .gremlin(canScrollGremlin)
  .gremlin(newCleanClickGremlin)
  .gremlin(newFormFillingGremlin);
  
  //Set seed for traceability
  horde.seed(1234);

  //Set distribution for gremlins to increase chance of using certain gremlins
  horde.strategy(window.gremlins.strategies.distribution()
    .delay(1)
    .distribution([0.05, 0.3, 0.65])
  )

  horde.after(callback);
  window.onbeforeunload = stop;
  setTimeout(stop, ttl);
  horde.unleash();
}

describe('Monkey testing with gremlins ', function() {

  it('it should not raise any error', function() {
    browser.url('/');
    browser.click('button=Cerrar');

    browser.timeoutsAsyncScript(60000);
    browser.executeAsync(loadScript);

    browser.timeoutsAsyncScript(60000);
    browser.executeAsync(unleashGremlins, 50000);
  });

  afterAll(function() {
    browser.log('browser').value.forEach(function(log) {
      browser.logger.info(log.message.split(' ')[2]);
    });
  });

});